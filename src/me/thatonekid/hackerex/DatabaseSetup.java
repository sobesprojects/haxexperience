package me.thatonekid.hackerex;

import org.bukkit.Bukkit;

import java.sql.*;
import java.util.UUID;

public class DatabaseSetup {

    public static HackerMain plugin = HackerMain.getPlugin(HackerMain.class);
    private static Connection con;
    public DatabaseSetup(){

    }

    //i cant code
    //what xbox never taught me
    /**xbox is a master coder**/

    public static void setCon(Connection connection) {
        try {
            con = connection;
            connection.isValid(5);
        } catch (SQLException ex) {
            Bukkit.getLogger()
                    .severe(ex.getSQLState() + " |||| " + ex.getMessage() + " |||| " + ex.getErrorCode());
            Bukkit.getLogger().severe("Hacker Experience> MySQL could not connect! Shutting server down.");
            Bukkit.getServer().getPluginManager().disablePlugin(plugin);
        }
    }

    public static Connection getCon(){
        return con;
    }

    public static boolean connect(){
        if (isConnected()){
            return true;
        }
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://" + HackerMain.mysql_host + ":" + HackerMain.mysql_port + "/" + HackerMain.mysql_database
                            + "?autoReconnect=true&verifyServerCertificate=false&useSSL=true",
                    HackerMain.mysql_user, HackerMain.mysql_pass);
            setCon(con);
            return true;
        }catch(Exception e){
            Bukkit.getLogger().severe(e.getMessage());
            Bukkit.getLogger().severe("-----[ Stacktrace ]----");
            e.printStackTrace();
            Bukkit.getLogger().severe("-----------------------");
            Bukkit.getLogger().severe("-----[ MySQL Information ]----");
            Bukkit.getLogger().severe("Host: " + HackerMain.mysql_host);
            Bukkit.getLogger().severe("Port: " + HackerMain.mysql_port);
            Bukkit.getLogger().severe("------------------------------");
            Bukkit.getLogger().severe("Hacker Experience> MySQL could not connect! Disabling plugin.");
            Bukkit.getServer().getPluginManager().disablePlugin(plugin);
            return false;
        }
    }

    public static void close() {
        if (isConnected()) {
            try {
                getCon().close();
                Bukkit.getLogger().info("MySQL successfully disconnected");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static ResultSet getResult(String qry) {
        if (isConnected()) {
            try {
                PreparedStatement ps = getCon().prepareStatement(qry);
                return ps.executeQuery();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            close();
            connect();
            try {
                PreparedStatement ps = getCon().prepareStatement(qry);
                return ps.executeQuery();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static boolean isConnected(){
        if (getCon() != null){
            return true;
        }
        return false;
    }
    public static boolean update(String qry) {
        if (isConnected()) {
            try {
                PreparedStatement ps = getCon().prepareStatement(qry);
                ps.executeUpdate();
                return true;
            } catch (SQLException e) {
                return false;
            }
        } else {
            close();
            connect();
            try {
                PreparedStatement ps = getCon().prepareStatement(qry);
                ps.executeUpdate();
                return true;
            } catch (SQLException e) {
                return false;
            }
        }
    }

    public static int updateresult(String qry) {
        if (isConnected()) {
            try {
                PreparedStatement ps = getCon().prepareStatement(qry);
                return ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                return 1;
            }
        } else {
            connect();
            try {
                PreparedStatement ps = getCon().prepareStatement(qry);
                return ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                return 1;
            }
        }
    }

    public static void createTable() {
        if (!isConnected()) {
            connect();
        }
        try {
            PreparedStatement create = getCon().prepareStatement(
                    "CREATE TABLE IF NOT EXISTS `players` ( `id` INT NOT NULL AUTO_INCREMENT , `player_name` VARCHAR(255) NOT NULL , `uuid` VARCHAR(100) NOT NULL , `ip` VARCHAR(100) NOT NULL , PRIMARY KEY (`id`));");
            create.executeUpdate();
        } catch (Exception ex) {
            Bukkit.getLogger().severe(ex.getMessage());
            Bukkit.getLogger().severe("-----[ Stacktrace ]----");
            ex.printStackTrace();
            Bukkit.getLogger().severe("-----------------------");
            Bukkit.getLogger().severe("Hacker Experience>  Could not create the MySQL Table!");
        }
    }

    public static String createPlayer(UUID uuid, String player, String ip){
        int rs = updateresult("INSERT INTO `players` (`player_name`, `uuid`, `ip`) " + "VALUES ('" + player
                + "','" + uuid + "','" + ip + "')");
        if (rs > 0){
            return "WORKED";
        } else {
            return "Tried to add \"" + player + "\" to the player database but failed! Check console.";
        }
    }

    public static boolean playerExists(UUID uuid){
        ResultSet rs = getResult("SELECT id FROM `players` WHERE uuid='" + uuid + "'");
        try{
            if (rs.next()){
                return true;
            }
            else {
                return false;
            }
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }

}
