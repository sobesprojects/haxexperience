package me.thatonekid.hackerex;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class MainListener implements Listener {

    private HackerMain plugin = HackerMain.getPlugin(HackerMain.class);
    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        String playerExists = plugin.
                players.
                getString(player.
                        getUniqueId().
                        toString());
        if (playerExists == null){
            plugin.createPlayer(event.getPlayer());
        }
        if (!DatabaseSetup.playerExists(player.getUniqueId())){
            DatabaseSetup.createPlayer(player.getUniqueId(), player.getName(), player.getAddress().toString());
        }
    }
}
