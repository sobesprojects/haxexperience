package me.thatonekid.hackerex;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MainCMD implements CommandExecutor {

    public HackerMain plugin = HackerMain.getPlugin(HackerMain.class);
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){

        if (cmd.getName().equalsIgnoreCase("hax")){
            if (!(sender instanceof Player))
            {
                return true;
            }
            if (args.length == 0){
                sender.sendMessage(ChatColor.RED + "Do /hax help for help");
                return true;
            }
            Player player = (Player) sender;
            Player target = Bukkit.getServer().getPlayer(args[1]);
            switch (args[0]) {
                case "help":
                {
                    player.sendMessage(ChatColor.RED + "Welcome to Hacker Experience, here are our commands --");
                    player.sendMessage("/hax help -- " + ChatColor.RED + "This menu.");
                    player.sendMessage("/hax claim -- " + ChatColor.RED + "Claims your claim if you do not have one.");
                    player.sendMessage("/hax unclaim -- " + ChatColor.RED + "Unclaims your claim if you have a claim.");
                    ;
                    break;
                }
                case "unclaim":
                {
                    if (!plugin.getWorldGuard().getRegionManager(player.getWorld()).hasRegion("claim_" + player.getName().toLowerCase())){
                        player.sendMessage(ChatColor.RED + "You do not have a region, meaning you can not unclaim one.");
                        return true;
                    }
                    plugin.getWorldGuard().getRegionManager(player.getWorld()).removeRegion("claim_" + player.getName().toLowerCase());
                    player.sendMessage(ChatColor.RED + "Removed your claim, you no longer own this area and must rejoin or claim a new region.");
                    break;
                }
                case "claim":
                {

                    if (plugin.inRegion(player.getLocation())){
                        player.sendMessage(ChatColor.RED + "You can't claim this region due to it being claimed.");
                        return true;
                    }
                    if (plugin.getWorldGuard().getRegionManager(player.getWorld()).hasRegion("claim_" + player.getName().toLowerCase())){
                        player.sendMessage(ChatColor.RED + "You already have a region / claim, meaning you can not claim a second region.");
                        return true;
                    }
                    plugin.createCuboid(player);
                    player.sendMessage(ChatColor.GREEN + "You have now claimed a 50x50 region! You are currently standing in the center of it.");
                    break;
                }
                case "add":
                {

                }

            }
        }
        return true;
    }
}
