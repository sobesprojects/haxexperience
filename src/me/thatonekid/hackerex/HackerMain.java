package me.thatonekid.hackerex;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.BukkitUtil;
import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.managers.storage.StorageException;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketPlayOutTitle;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

public class HackerMain extends JavaPlugin implements Listener {

    public static HackerMain plugin;
    public File playerData;
    public FileConfiguration players;

    public static String mysql_host, mysql_user, mysql_pass, mysql_database;
    public static int mysql_port;

    @Override
    public void onEnable()
    {
        if (!getDataFolder().exists()){
            getDataFolder().mkdirs();
        }
        playerData = new File(getDataFolder(), "players.yml");
        players = YamlConfiguration.loadConfiguration(playerData);
        if (!playerData.exists())
        {
            try {
                playerData.createNewFile();
                createPlayersConfig();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        getConfig().options().copyDefaults(true);
        saveConfig();

        mysql_host = getConfig().getString("mysql.host");
        mysql_pass = getConfig().getString("mysql.password");
        mysql_user = getConfig().getString("mysql.user");
        mysql_port = getConfig().getInt("mysql.port");
        mysql_database = getConfig().getString("mysql.database");

            DatabaseSetup.connect();
            DatabaseSetup.createTable();

        getServer().getPluginManager().registerEvents(this, this);
        getServer().getPluginManager().registerEvents(new MainListener(), this);
        getCommand("hax").setExecutor(new MainCMD());
    }

    @Override
    public void onDisable()
    {
        DatabaseSetup.close();
    }
    public boolean hasRegion(Player p)
    {
        if (getWorldGuard().getRegionManager(p.getWorld()).hasRegion("claim_" + p.getName()))
        {
            return true;
        }
        return false;
    }
    public static int random(int min, int max)
    {
        int range = max - min + 1;
        int value = (int) (Math.random() * range) + min;
        return value;
    }

    public void teleportRandom(Player player)
    {
        World world = getServer().getWorlds().get(0);

        int x = random(-100000, 100000);
        int z = random(-100000, 100000);
        int y = player.getWorld().getHighestBlockYAt(x, z);
        Location location = new Location(world, x, y, z);
        if (location.getBlock().isLiquid())
        {
            player.teleport(location);

        }
        for (ProtectedRegion region : getWorldGuard().getRegionManager(player.getWorld()).getApplicableRegions(location))
        {
            if (region.hasMembersOrOwners() && region.getId() != "claim_" + player.getName())
            {
                player.teleport(location);
            }
        }


    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        if (!hasRegion(player))
        {
            teleportRandom(player);
            createCuboid(player);
        }
        if (player.hasPlayedBefore())
        {
            Bukkit.dispatchCommand(getServer().getConsoleSender(), "title " + player.getName() + " title {\"text\":\"Welcome Back to HackerEx!\",\"color\":\"red\"}");
        }
        if (!player.hasPlayedBefore())
        {
            Bukkit.dispatchCommand(getServer().getConsoleSender(), "title " + player.getName() + " title {\"text\":\"Welcome to HackerEx!\",\"color\":\"red\"}");
        }

    }

    @EventHandler
    public void onInteractBed(PlayerInteractEvent event)
    {
        Block block = event.getClickedBlock();
        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && block.getType() == Material.BED_BLOCK)
        {
            event.getPlayer().setBedSpawnLocation(block.getLocation());
            event.getPlayer().sendMessage(ChatColor.YELLOW + "Spawn point set.");
        }

    }

    public boolean inRegion(Location loc)
    {
        WorldGuardPlugin guard = getWorldGuard();
        Vector v = BukkitUtil.toVector(loc);
        RegionManager manager = guard.getRegionManager(loc.getWorld());
        ApplicableRegionSet set = manager.getApplicableRegions(v);
        return set.size() > 0;
    }


    public void createCuboid(Player p)
    {
        World pLocation = p.getLocation().getWorld();
        int regionSize = 50;
        double X = p.getLocation().getX();
        double Z = p.getLocation().getZ();
        double x1 = X - regionSize;
        double y1 = 0;
        double z1 = Z - regionSize;
        double x2 = X + regionSize;
        double y2 = p.getWorld().getMaxHeight();
        double z2 = Z + regionSize;
        BlockVector min = new BlockVector(x1, y1, z1);
        BlockVector max = new BlockVector(x2, y2, z2);
        ProtectedRegion region = new ProtectedCuboidRegion("claim_" + p.getName(), false, min, max);
        RegionContainer container = getWorldGuard().getRegionContainer();
        RegionManager regions = container.get(pLocation);
        regions.addRegion(region);
        DefaultDomain owners = new DefaultDomain();
        owners.addPlayer(p.getUniqueId());
        region.setOwners(owners);
        region.getFlags().clear();
        region.setFlag(DefaultFlag.BUILD, StateFlag.State.DENY);
        p.setBedSpawnLocation(new Location(p.getLocation().getWorld(), p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ()));
        p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10, 10);
        try {
            regions.save();
        } catch (StorageException e) {
            e.printStackTrace();
        }
        if (p.getInventory().contains(new ItemStack(Material.BED)) || p.getInventory().contains(new ItemStack(Material.BED_BLOCK))){
            p.getInventory().remove(new ItemStack(Material.BED));
            p.getInventory().remove(new ItemStack(Material.BED_BLOCK));
        }
        p.getInventory().addItem(new ItemStack(Material.BED, 1));



    }


    //WorldGuard
    @SuppressWarnings("unused")
    public WorldGuardPlugin getWorldGuard() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
        return (WorldGuardPlugin) plugin;
    }

    //WorldEdit
    @SuppressWarnings("unused")
    public WorldEditPlugin getWorldEdit() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
        return (WorldEditPlugin) plugin;
    }



    public void createPlayer(Player player){
        players.createSection(player.getUniqueId().toString());
        players.set(player.getUniqueId().toString() + ".name", player.getName());
        players.set(player.getUniqueId().toString() + ".ip", "Unknown");
        savePlayers();

    }

    public void createPlayersConfig(){
        players.createSection("players");
        try {
            players.save(playerData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void savePlayers(){
        try {
            players.save(playerData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
